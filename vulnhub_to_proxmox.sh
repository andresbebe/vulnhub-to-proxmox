#!/bin/bash

#Este script se realizo para zfs con la version de proxmox  6.3.3. 

trap "exit 1" TERM
export TOP_PID=$$

function adios()
{
   printf "Saliendo del programa."
   kill -s TERM $TOP_PID
}

printf "Ingrese el archivo de la VM a cargar:"

read filename

fname="${filename%.*}"

function desc_gen(){
        if [ "${filename##*.}" = "ova" ]; then
                printf "Creando directorio $fname"
                mkdir $fname
                tar -xvf $filename -C $fname/
        else 
                echo "No se encuentra el ova"
                adios
        fi
}
desc_gen $filename

function obt_vmid(){
        printf "Obteniendo Vmid"
        vmid=$(pvesh get /cluster/nextid)
        printf $vmid
}

obt_vmid

printf "Comprobando si existe vmdk"

ruta=$(pwd)
ruta2="$ruta/$fname/"
cd $ruta2
file=$(find ./ -name *.vmdk) 

function vm_create(){

if [ "${file##*.}" == "vmdk" ]; then
        printf "Creando vm con el nombre $fname y el vmid $vmid"
        qm create $vmid --name $fname --net0 virtio=62:57:BC:A2:0E:18,bridge=vmbr0,firewall=1 --scsi0 local-zfs:4  --ostype l26 --memory 1024
        printf "Detach del disco de la vm"
        qm set $vmid  -delete scsi0
        printf "Borrando disco de la vm"
        qm set $vmid  -delete unused0
else
        printf "No se encuentra vmdk"
        adios
fi
sleep 5
}

vm_create

printf "Importando disco a proxmox a la VM"

qm importdisk $vmid $file local-zfs -format qcow2

printf "Haciendo el atach del disco a la VM"

qm set $vmid  --scsi0 local-zfs:vm-$vmid-disk-0

printf "Cambiando el orden de booteo de la VM"

qm set $vmid --boot c --bootdisk scsi0
exit

