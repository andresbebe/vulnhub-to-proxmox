**Vulnhub to Proxmox**

Es un script en bash que permite la creación y carga de archivos OVAs  de Vulnhub a el KVM de Proxmox.

**¿Qué es lo que hace el script?**

1. Al ingresar la ruta del OVA
2. Hace un TAR del OVA,comprueba la existencia del vmdk.
3. Crea un directorio con el nombre de la maquina
4. Obtiene el proximo vm id en Proxmox
5. Crea una vm con ese id, hace un dettach y elimina el hd por defecto creado.
6. Importa el vmdk a qcow2, y hace el attach del disco y cambia el orden de booteo por defecto en la VM.

- Este script fue pensado para un filesystem con ZFS con una configuracion por defecto de proxmox.
- Proximamente lo estare modificando para que use lvm.

**Features posibles a futuro:**

- Posibilidad de agregar archivos gz,tar,rar,zip.
- Soporte lvm.
- Ver la posiblidad de crear algun downloader y registro, alta y eliminacion de las maquinas bajadas.
- Virtualizacion de otros dispositivos en proxmox


